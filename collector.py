# SPDX-FileCopyrightText: 2021 Mario Fux <mario@unormal.org>
#￼ SPDX-License-Identifier: GPL-2.0-or-later


import datetime
import json
import mysql.connector
import pprint
import requests
from io import BytesIO
from lxml import etree
from SPARQLWrapper import SPARQLWrapper, JSON
from speciesnamegenerator import createSpeciesName
from picturehelper import createCopyrightLine, scrapPictures

# Temporary workaround for SSL problem with dbpedia
# (Source: https://stackoverflow.com/questions/50236117/scraping-ssl-certificate-verify-failed-error-for-http-en-wikipedia-org)
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

###############################
# TODO LIST
###############################
# 0th run: clean up code and create modules and create DB schema
# 1st run: download all species with species and genus and create unique species names (query0 below)
# 2nd run: download all metadata for the species (resources): (query5 below)
# 2.1 run: - go through json results one by one and check if table exists (for different languages (create a list for tables that exists and check if table in list) or create it and add information
# 3rd run: download all pictures of a species' (resource) wikipage (and their languages), add their file names to the db (and link them to the resource/species) and add their captions to the db
# 3.1 run: - create the copyright lines for the pictures and add them to the DB (make a list of licences in the DB)
# 4th run: Get data from other dbpedias (other languages) or work on PHP and JS or QML code
# One script (.py file) for each run


###############################
# DB data and connection
# TODO: Create DB class
mydb = mysql.connector.connect(
  host="localhost",
  user="dbadmin",
  password="db123",
  database="flauna"
)
mycursor = mydb.cursor()

dbpediaUser = 23

# DIFFERENT QUERIES
query0 = """SELECT DISTINCT ?p ?genus (group_concat(distinct ?species;separator=" | ") as ?species) {?p rdf:type dbo:Species . ?p dbp:genus ?genus . ?p dbp:species ?species } LIMIT 10000 OFFSET 20000"""
#Species (english) with (r, label,) genus and species and optional German label:
query = "SELECT ?p ?label ?genus ?species ?label2 {?p rdf:type dbo:Species . ?p rdfs:label ?label . ?p dbp:genus ?genus . ?p dbp:species ?species . OPTIONAL { ?p rdfs:label ?label2 . FILTER (LANG(?label2) = 'de') }. FILTER (LANG(?label) = 'en') } LIMIT 10"
# 137322 (without just genus thus without dbp:species)
query2 = "SELECT ?p ?label ?genus ?species ?label2 ?regnum ?phylum ?classis ?ordo ?familia ?tribus ?thumbnail {?p rdf:type dbo:Species . ?p rdfs:label ?label . ?p dbp:genus ?genus . ?p dbp:species ?species . OPTIONAL {?p dbp:regnum ?regnum } . OPTIONAL {?p dbp:phylum ?phylum } . OPTIONAL {?p dbp:classis ?classis } . OPTIONAL {?p dbp:ordo ?ordo } . OPTIONAL {?p dbp:familia ?familia } . OPTIONAL {?p dbp:tribus ?tribus } . OPTIONAL {?p dbo:thumbnail ?thumbnail } . OPTIONAL { ?p rdfs:label ?label2 . FILTER (LANG(?label2) = 'de') }. FILTER (LANG(?label) = 'en') } LIMIT 10"
# Number of distinct species in english
query3 = """SELECT count distinct ?Species where { ?Species rdf:type dbo:Species . ?Species rdfs:label ?label . FILTER (LANG(?label) = 'en')}"""


query4 = """SELECT ?p
                   ?label_en ?label_de ?label_fr ?label_it
                   ?abstract_en ?abstract_de ?abstract_fr ?abstract_it
                   ?comment_en ?comment_de ?comment_fr ?comment_it
                   ?genus ?species ?regnum ?phylum ?classis ?ordo ?familia ?tribus ?thumbnail
            WHERE {?p rdf:type dbo:Species .
                   ?p rdfs:label ?label_en .
                   ?p rdfs:comment ?comment_en .
                   ?p dbo:abstract ?abstract_en .
                   ?p dbp:genus ?genus .
                   ?p dbp:species ?species .
                   OPTIONAL {?p dbp:regnum ?regnum } .
                   OPTIONAL {?p dbp:phylum ?phylum } .
                   OPTIONAL {?p dbp:classis ?classis } .
                   OPTIONAL {?p dbp:ordo ?ordo } .
                   OPTIONAL {?p dbp:familia ?familia } .
                   OPTIONAL {?p dbp:tribus ?tribus } .
                   OPTIONAL {?p dbo:thumbnail ?thumbnail } .
                   OPTIONAL { ?p rdfs:label ?label_de . FILTER (LANG(?label_de) = 'de') } .
                   OPTIONAL { ?p rdfs:label ?label_fr . FILTER (LANG(?label_fr) = 'fr') } .
                   OPTIONAL { ?p rdfs:label ?label_it. FILTER (LANG(?label_it) = 'it') } .
                   OPTIONAL { ?p rdfs:comment ?comment_de . FILTER (LANG(?comment_de) = 'de') } .
                   OPTIONAL { ?p rdfs:comment ?comment_fr . FILTER (LANG(?comment_fr) = 'fr') } .
                   OPTIONAL { ?p rdfs:comment ?comment_it. FILTER (LANG(?comment_it) = 'it') } .
                   OPTIONAL { ?p dbo:abstract ?abstract_de . FILTER (LANG(?abstract_de) = 'de') } .
                   OPTIONAL { ?p dbo:abstract ?abstract_fr . FILTER (LANG(?abstract_fr) = 'fr') } .
                   OPTIONAL { ?p dbo:abstract ?abstract_it. FILTER (LANG(?abstract_it) = 'it') } .
                   FILTER (LANG(?abstract_en) = 'en') .
                   FILTER (LANG(?comment_en) = 'en') .
                   FILTER (LANG(?label_en) = 'en') }"""
                   
                   
query5 = "SELECT DISTINCT ?property ?object { dbr:Salix_alba ?property ?object}"

# PREDICATES
# TODO: What about several families or order (e.g. https://dbpedia.org/page/Catananche_caerulea) => dbp: seems single
#- Hierarchy (non-i18n):
#Kingdom:   dbo:kingdom - dbp:regnum - reduction by some 100 => OPTIONAL
#(Clade:)
#Phylum:    dbo:phylum - dbp:phylum - reduction by some 1000 => OPTIONAL
#Class:     dbo:class - dbp:classis - reduction by some 1000 => OPTIONAL
#Order:     dbo:order - dbp:ordo - reduction by some 1000 => OPTIONAL
#Family:    dbo:family - dbp:familia - reduction by some 100 => OPTIONAL
#Subfamily:
#Tribe:     - dbp:tribus - reduction by quite some 10000 => OPTIONAL
#Subtribe:
#Genus:     dbo:genus - dbp:genus
#Species:   dbo:species - dbp:species
#Binomial name: dbp:binomial
#Subspecies: dbp:subspecies
#Trinomial: dbp:trinomial

# TODO:
#If species is double
#-> check for dbp:cultivar (Apples -> Malus domestica e.g. https://dbpedia.org/page/Harrison_Cider_Apple) or dpb:hybrid (e.g. https://dbpedia.org/page/Aurora_Golden_Gala)
#-> check for dbp:subspecies and/or dbp:trinomial (Prunus e.g. https://dbpedia.org/page/Mirabelle_plum)

# TODO: Check for dbp:binomial and see if it's our speciesname?


#Probably use regnum, phylum, classis, ordo, familia, tribus, genus and species at first
#- Pictures (non-i18n):   
#Thumbnail: dbo:thumbnail
#Picture: foaf:depiction
#dbp:rangeMap
#dbp:rangeMapCaption
#- Info (i18n):
#Label: rdfs:label
#Comment: rdfs:comment
#Categories: dct:subject
#Abstract: dbo:abstract
#dbp:imageCaption
#- Info (non-i18n):
#Wikipage: prov:wasDerivedFrom - no reduction 
#SameAs: owl:sameAs

# SPARQL query
sparql = SPARQLWrapper("https://dbpedia.org/sparql")

# Query DBPedia with SPARQL
sparql.setQuery(query0)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

# DEBUG: Write result to json file
#a = open("result2.json", "w")
#a.write(str(results))
#a.close()
# DEBUG: Read resulft from json file
#a = open("example.json", "r")
#results = a.read()
#results = json.loads(results)
#a.close()

#pprint.pprint(results)


def getResourcesProperties(resource):
    query = "SELECT DISTINCT ?property ?object { <" + resource +  "> ?property ?object}"
    
    print(query)
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")

    # Query DBPedia with SPARQL
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    pprint.pprint(results)

#getResourcesProperties("http://dbpedia.org/resource/Mirabelle_plum")
#quit()

#url = "https://rm.wikipedia.org/wiki/Salesch_alv"
#scrapPictures(url)

#################################################
# Helper function for insertion into sql database
def insertIntoDb(fid, table, value):
    
    sql = "INSERT INTO " +  table + " (fid, value, orgUserId, orgDate) VALUES (%s, %s, %s, NOW())"
    val = (fid, value, dbpediaUser)
    mycursor.execute(sql, val)
    mydb.commit()
    print("LastRowID: " + str(mycursor.lastrowid))


#################################################
# Helper function for insertion into sql database
def queryToPPrint(query):
    sparql = SPARQLWrapper("https://dbpedia.org/sparql")

    # Query DBPedia with SPARQL
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    
    result = results["results"]["bindings"]
    
    if 'subspecies' in result[0]:
        print("Yes")
        
    
    pprint.pprint(results)
    pprint.pprint(result[0])

deduplicateQuery = "SELECT ?cultivar ?subspecies ?trinomial { OPTIONAL { <http://dbpedia.org/resource/Mirabelle_plum> dbp:cultivar ?cultivar } . OPTIONAL { <http://dbpedia.org/resource/Mirabelle_plum> dbp:subspecies ?subspecies } . OPTIONAL { <http://dbpedia.org/resource/Mirabelle_plum> dbp:trinomial ?trinomial } }"
queryToPPrint(deduplicateQuery)

#############################################
# Iterate over the results, extract information and store it in the db
logFile = "error.log"
log = open(logFile, "a")
log.write("NEW RUN\n")


#i = 30000
#for result in results["results"]["bindings"]:
    #i += 1
    
    ##if "regnum" in result:
        ##regnum = result["regnum"]["value"]
    ##if "phylum" in result:
        ##phylum = result["phylum"]["value"]
    ##if "classis" in result:
        ##classis = result["classis"]["value"]
    ##if "ordo" in result:
        ##ordo = result["ordo"]["value"]
    ##if "familia" in result:
        ##familia = result["familia"]["value"]
    ##if "tribus" in result:
        ##tribus = result["tribus"]["value"]
    ##if "thumbnail" in result:
        ##thumbnail = result["thumbnail"]["value"]
        ##print("Thumbnail: ")# + createCopyrightLine(thumbnail))
    
    #fid = i
    
    #genus = result["genus"]["value"]
    #if not genus:
        #log.write(str(datetime.datetime.now()) + " - " + resource + " genus is empty - " + str(genus) + "\n")
        #continue
    
    #resource = result["p"]["value"]
    #if not resource:
        #log.write(str(datetime.datetime.now()) + " - " + resource + " resource is empty - " + str(resource) + "\n")
        #continue
    
    ## Check for species name
    #speciesList = result["species"]["value"]
    #if not speciesList:
        #log.write(str(datetime.datetime.now()) + " - " + resource + " species is empty - " + str(speciesList) + "\n")
        #continue
    #if " | " in speciesList:
        #species = speciesList.split(" | ")[0]
        #print("Species: " + species)
        
        #if not species:
            #species = speciesList.split(" | ")[1]
            #if not species:
                #log.write(str(datetime.datetime.now()) + " - " + resource + " has empty speciesList - " + str(speciesList) + "\n")
                #continue
    #else:
        #species = speciesList
    
    
    
    ## TODO: Workarounds for resources with problems: (recheck later)
    ## 20210211: http://dbpedia.org/resource/Carychium_pessimum <- species fixed
    ## 20210211: http://dbpedia.org/page/Copelatus_tenebrosus <- species fixed
    ## 20210211: http://dbpedia.org/page/Coprinellus_mitrinodulisporus <- species fixed
    ## 20210211: http://dbpedia.org/resource/Xenorhabdus_kozodoii <- species, name and binomial name fixed
    #wrongSpeciesList = ["http://dbpedia.org/resource/Carychium_pessimum",
                 #"http://dbpedia.org/resource/Copelatus_tenebrosus",
                 #"http://dbpedia.org/resource/Coprinellus_mitrinodulisporus",
                 #"http://dbpedia.org/resource/Xenorhabdus_kozodoii"]
    #if resource in wrongSpeciesList:
        #print("EEEEEEEEEEEEEEch...")
        #continue
    
    ##fruitWrongList = ["http://dbpedia.org/resource/Jubilee_apple", "http://dbpedia.org/resource/Stobrawa_potato", "http://dbpedia.org/resource/Carrie_(mango)", "http://dbpedia.org/resource/Dorsett_Golden", "http://dbpedia.org/resource/Sunset_(mango)", "http://dbpedia.org/resource/Rooster_potato", "http://dbpedia.org/resource/Graham_(mango)", "http://dbpedia.org/resource/Allington_Pippin", "http://dbpedia.org/resource/Detroit_Red_(apple)", "http://dbpedia.org/resource/Vivaldi_potato"]
    ##if resource in fruitWrongList:
        ##print("EEEEEEEEEEEEEEch...")
        ##log.write(str(datetime.datetime.now()) + " - " + resource + " double\n")
        ##continue

    ## Double entries...
    ##if "http://dbpedia.org/resource/Coregonus_baicalensis" in resource:
        ##print("EEEEEEEEEEEEEEch...")
        ##continue
    ##if "http://dbpedia.org/resource/Coregonus_bezola" in resource:
        ##print("EEEEEEEEEEEEEEch...")
        ##continue        
    
    ## select * from flauna where name = "Coprinellus_pellucidus"
    
    ## Debug output
    #print(str(i) + ". " + "entry: " + resource)
    #print(genus + " # " + species)
    
    
    ##print("Label: " + label)
    #speciesName = createSpeciesName(genus, species, resource)
    
    ## TODO: Check if there is already an entry in the db with this speciesname than try with subspecies and if not log it
    #sql = "SELECT * FROM flauna WHERE name = %s"
    #val = (speciesName, )
    #mycursor.execute(sql, val)
    #myresult = mycursor.fetchall()
    
    #if len(myresult) > 0:
        #log.write(str(datetime.datetime.now()) + " - " + resource + " double - " + speciesName + " - " + str(myresult[0]) + "\n")
        ## TODO: Check for cultivar or subspecies/trinomial and then change species and species of other DB entry
        #deduplicateQuery = "SELECT ?cultivar ?subspecies ?trinomial { OPTIONAL { <" + resource + "> dbp:cultivar ?cultivar } . OPTIONAL { <" + resource + "> dbp:subspecies ?subspecies } . OPTIONAL { <" + resource + "> dbp:trinomial ?trinomial } }"
        #sparql.setQuery(deduplicateQuery)
        #sparql.setReturnFormat(JSON)
        #results = sparql.query().convert()
        #pprint.pprint(results)
        
        #if "subspecies" in results:
            #print("Bla")
        #elif "trinomial" in results:
            #print("Blu")
        #elif "cultivar" in results:
            #print("Bli")
        
        #continue
    
    #print("Species name: " + speciesName)
    
    
    #sql = "INSERT INTO flauna (fid, name, dbpediaResource, userId, date) VALUES (%s, %s, %s, %s, NOW())"
    #val = (fid, speciesName, resource, dbpediaUser)
    #mycursor.execute(sql, val)
    #mydb.commit()
    #print("LastRowID: " + str(mycursor.lastrowid))

    ###TODO: For single resource scrapping
    ##if result["property"]["value"] == "http://dbpedia.org/property/genus":
        ##print ("Genus: " + result["object"]["value"])
        
    ##if result["property"]["value"] == "http://dbpedia.org/property/species":
        ##print ("Species: " + result["object"]["value"])
        
    ##if result["property"]["value"] == "http://dbpedia.org/ontology/abstract":
        ##print("Abstract: " + result["object"]["xml:lang"])

    ##if result["property"]["value"] == "http://www.w3.org/2000/01/rdf-schema#comment":
        ##print("Comment: " + result["object"]["xml:lang"])
    
    ###Generate and execute SQL INSERT queries
    #insertIntoDb(fid, "species", species)
    #insertIntoDb(fid, "genus", genus)
    



#log.close()











#for result in results["results"]["bindings"]:
    #no = int(result["callret-0"]["value"])
    #print("Number of Species: " + str(no))

    ##print(str(i))
    #i = i + 1
    

#sparql.setQuery("""SELECT ?p ?label {?p dbo:class dbr:Insect . ?p rdfs:label ?label . FILTER (LANG(?label) = 'de') }""");





#i = 1

#for x in range(0, 20000, 10000):
    ##query = "SELECT ?p ?label {?p rdf:type dbo:Species . ?p rdfs:label ?label . FILTER (LANG(?label) = 'en')} LIMIT 10000 OFFSET " + str(x)
    #query = "SELECT ?p ?label ?kingdom {?p rdf:type dbo:Species . ?p rdfs:label ?label . OPTIONAL {?p dbo:kingdom ?kingdom} . FILTER (LANG(?label) = 'en')} LIMIT 10000 OFFSET " + str(x)

    #sparql.setQuery(query);
    #sparql.setReturnFormat(JSON)
    #results = sparql.query().convert()

    #for result in results["results"]["bindings"]:
        ######################
        ## Some output of data
        ##print(str(i) + " " + result["label"]["value"])
        ##print(str(i) + " " + result["p"]["value"])
        ##print(str(i) + " " + result["label"]["xml:lang"])
        
        #################################
        ## Database insertion of the data
        #sql = "INSERT INTO sparqltest (p, label) VALUES (%s, %s)"
        #val = (result["p"]["value"], result["label"]["value"])
        ##mycursor.execute(sql, val)

        ##mydb.commit()
        ##print(mycursor.rowcount, "record inserted.")
        

        ###########################
        ## Log SQL line in log file
        
        ######################################
        ## Check if the label is in German ;-)
        ##if result["label"]["xml:lang"] == "de" :
            ##print("Yuhuu")
            ##print(str(i) + " " + result["label"]["value"])
            ##print()
        
        ###############
        ## Dump of data
        ##print(str(i) + " " + json.dumps(result))

        #if "kingdom" in result:
            #if str(result["kingdom"]["value"]).endswith("Fungus"):
                #print("Pilz!!!")
                #print(str(i) + " " + result["p"]["value"])


        #i = i + 1


