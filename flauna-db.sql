CREATE DATABASE IF NOT EXISTS `flauna` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
USE `flauna`;


-- TODO: dbpediaResource needs to go to another separate table (as not all date in the future will come from dbpedia)


CREATE TABLE `flauna` (
  `fid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dbpediaResource` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `userId` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

ALTER TABLE `flauna`
  ADD PRIMARY KEY (`fid`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `flauna`
  MODIFY `fid` int(11) NOT NULL AUTO_INCREMENT;

  
CREATE TABLE `genus` (
  `id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `orgUserId` int(11) NOT NULL,
  `orgDate` timestamp NOT NULL,
  `r1UserId` int(11) DEFAULT NULL,
  `r1Date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `r2UserId` int(11) DEFAULT NULL,
  `r2Date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `timeCategory` int(11) DEFAULT NULL,
  `lastUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

ALTER TABLE `genus`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `genus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `species` (
  `id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `orgUserId` int(11) NOT NULL,
  `orgDate` timestamp NOT NULL,
  `r1UserId` int(11) DEFAULT NULL,
  `r1Date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `r2UserId` int(11) DEFAULT NULL,
  `r2Date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `timeCategory` int(11) DEFAULT NULL,
  `lastUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

ALTER TABLE `species`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `species`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
