

######################################
# Helper function for the creation of a distinct species name (binomial name)
def createSpeciesName(genus, species, resource):
    # TODO: Use binomial name if there (or at least check if it matches with our creation)
    speciesName = ""
    resourceName = resource.split("/")[-1]
    
    # Remove "†" for extinct (?) species and strip the strings
    # TODO: Might wanna set a variable for dead species
    genus = genus.replace("†", "").strip()
    species = species.replace("†", "").strip()
        
    # Remove URLs and underscore separated parts from genus
    # TODO: Could do this in another way (check if type is "uri" instead of "literal"
    if genus.startswith("http://dbpedia.org/resource"):
        genus = genus.split("/")[-1]
        if "_" in genus:
            genus = genus.split("_")[0]
        #print("Genus: " + genus)
    
    # Check if first letter of genus matches first letter of species
    if genus[0].lower() == species[0].lower():
        speciesName = genus + species[2:]
        # TODO: needs to be decided if I want to change space " " to underscore "_"
        speciesName = speciesName.replace(" ", "_")
    else:
        # TODO: Maybe add a flag for the case the above doesn't work
        speciesName = genus + "_" + species
        
    # TODO: Check if speciesName is part of last part of URL of resource and if this part is larger take it (or check for subspecies)
    if ( speciesName in resourceName ) and ( len(speciesName) < len(resourceName) ):
        speciesName = resourceName
        # query = "SELECT resource dbp:subspecies"

    return speciesName
